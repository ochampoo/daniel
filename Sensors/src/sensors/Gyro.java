package sensors;

import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.Motor;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class Gyro {
	public static void main(String[] args){
		EV3GyroSensor gyroSensor = new EV3GyroSensor(SensorPort.S3);
		SampleProvider sp1 = gyroSensor.getAngleMode();
		SampleProvider sp2 = gyroSensor.getRateMode();
		float[] sample = new float[sp1.sampleSize()];
		float[] sample2 = new float[sp2.sampleSize()];
		
		
		
		while(Button.ESCAPE.isUp()) {
			
			Motor.A.forward();
			Motor.B.forward();
			Motor.A.setSpeed(360);
			Motor.B.setSpeed(360);
			Delay.msDelay(2000);
			Motor.A.rotateTo(180);
			sp1.fetchSample(sample, 0);
			sp2.fetchSample(sample2, 0);
			LCD.clear(4);
			LCD.drawString("groy: " + sample[0],0,3);
			LCD.clear(4);
			LCD.drawString("groy: " + sample2[0],0,4);
			break;
			}
		
		
		gyroSensor.close();
		
	}

}
