package sensors;

import java.io.IOException;

import lejos.hardware.Button;
import lejos.hardware.Sounds;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.BaseRegulatedMotor;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.Motor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor; 
import lejos.robotics.RegulatedMotor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import lejos.hardware.Sound;
public class Bumper {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		EV3TouchSensor touchSensor = new EV3TouchSensor(SensorPort.S2);
		SampleProvider sp = touchSensor.getTouchMode();
		float[] sample = new float[sp.sampleSize()];
		
		 while(Button.ESCAPE.isUp()) {
	         sp.fetchSample(sample, 0);
	         if (sample[0] == 0){
	        	 Motor.B.setSpeed(360);
				Motor.C.setSpeed(360);
				Motor.B.forward();
				Motor.C.forward();
	         }
	         else{
	        	 while(Button.ESCAPE.isUp()){
	        	 Sound.twoBeeps();
	        	 }
	         }
	      }
	      
	      touchSensor.close();
	     
		
		

	}

}
