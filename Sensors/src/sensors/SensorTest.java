package sensors;

import java.io.IOException;
import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.BaseRegulatedMotor;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor; 
import lejos.robotics.RegulatedMotor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

// Worked with HUDAC 

public class SensorTest {
		public static void main(String[] args) throws IOException {
			float frequency = 1; // 1 sample per second
		
	/*
			EV3TouchSensor touchSensor = new EV3TouchSensor(SensorPort.S2);
			SampleProvider sp = touchSensor.getTouchMode();
			float[] sample = new float[sp.sampleSize()];
		*/	
			EV3UltrasonicSensor sonicSensor = new EV3UltrasonicSensor(SensorPort.S4);
			SampleProvider sp2 = sonicSensor.getDistanceMode();
			float[] sample2 = new float[sp2.sampleSize()];
			
			/*
			EV3GyroSensor gyroSensor = new EV3GyroSensor(SensorPort.S3);
			SampleProvider sp3 = gyroSensor.getRateMode();
			float[] sample3 = new float[sp3.sampleSize()];
			
			EV3ColorSensor colorSensor = new EV3ColorSensor(SensorPort.S1);
			SampleProvider sp4 = colorSensor.getColorIDMode();
			float[] sample4 = new float[sp4.sampleSize()];
			
			*/
			
			// until the escape button is pressed
			while(Button.ESCAPE.isUp()) {
				//sp.fetchSample(sample, 0);
				//LCD.clear(3);
			//	LCD.drawString("Touch: " + sample[0],0,3);
				sp2.fetchSample(sample2, 0);
				LCD.clear(4);
				LCD.drawString("Sonic: " + sample2[0],0,4);
			//	sp3.fetchSample(sample3, 0);
			//	LCD.clear(5);
			//	LCD.drawString("Gyro: " + sample3[0],0,5);
				Delay.msDelay((long) (1000/frequency));
			}
			
		//	touchSensor.close();
			sonicSensor.close();
		//	gyroSensor.close();
			
			
			
		}
}
