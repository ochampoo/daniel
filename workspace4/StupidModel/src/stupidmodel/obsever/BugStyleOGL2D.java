package stupidmodel.obsever;


public class BugStyleOGL2D extends DefaultStyleOGL2D {
        @Override
        public Color getColor(final Object agent) {
                if (agent instanceof Bug) {
                        final Bug bug = (Bug) agent;

                        final int strength = (int) Math.max(200 - 20 * bug.getSize(), 0);
                        return new Color(0xFF, strength, strength); // 0xFFFFFF - white,
                                                                    // 0xFF0000 - red
                }

                return super.getColor(agent);
        }
