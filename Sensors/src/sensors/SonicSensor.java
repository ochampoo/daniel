package sensors;


import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.Motor;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class SonicSensor {
	public static void main(String[] args){
	

				EV3UltrasonicSensor sonicSensor = new EV3UltrasonicSensor(SensorPort.S4);
				SampleProvider sp2 = sonicSensor.getDistanceMode();
				float[] sample2 = new float[sp2.sampleSize()];
				double distance;
				

				
				
				while(Button.ESCAPE.isUp()){
					
					sp2.fetchSample(sample2, 0);
					distance = sample2[0];
					Motor.B.setSpeed(360);
					Motor.C.setSpeed(360);
					Motor.B.forward();
					Motor.C.forward();		
					LCD.clear(4);
					LCD.drawString("Sonic: " + sample2[0],0,3);
					if(distance<=0.1){
						Motor.B.setSpeed(360);
						Motor.C.setSpeed(360);
						Motor.B.backward();
						Motor.C.backward();
						Delay.msDelay(2000);
						break;
					}
				}
				sonicSensor.close();
				
			}



	}

